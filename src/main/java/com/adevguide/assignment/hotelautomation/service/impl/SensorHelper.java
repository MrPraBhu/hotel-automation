/**
 * 
 */
package com.adevguide.assignment.hotelautomation.service.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.adevguide.assignment.hotelautomation.model.SubCorridor;

/**
 * @author PraBhu
 *
 */
@Component
public class SensorHelper {

	private static final Logger log = LoggerFactory.getLogger(SensorHelper.class);

	@Value("${hotel.on-timer}")
	private long timer;

	@Async
	public void managePowerConsumption(SubCorridor currentSubCorridor, SubCorridor acOffSubcorridor) {

		try {
			acOffSubcorridor.getAc().setValue(false);
			log.info("Thread will sleep for a {}  minute(s).", timer);
			TimeUnit.MINUTES.sleep(timer);
			log.info("Resuming execution after sleep for a {}  minute(s).", timer);
			currentSubCorridor.getLight().setValue(false);
			acOffSubcorridor.getAc().setValue(true);

		} catch (InterruptedException e) {
			log.error("Exception Occured while managePowerConsumption.", e);
		}

	}

}
