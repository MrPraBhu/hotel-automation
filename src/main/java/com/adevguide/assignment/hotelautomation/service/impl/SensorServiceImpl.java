/**
 * 
 */
package com.adevguide.assignment.hotelautomation.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.adevguide.assignment.hotelautomation.model.Floor;
import com.adevguide.assignment.hotelautomation.model.HotelState;
import com.adevguide.assignment.hotelautomation.model.MainCorridor;
import com.adevguide.assignment.hotelautomation.model.SensorInputDTO;
import com.adevguide.assignment.hotelautomation.model.SubCorridor;
import com.adevguide.assignment.hotelautomation.service.SensorService;

/**
 * @author PraBhu
 *
 */
@Service
public class SensorServiceImpl implements SensorService {

	private static final Logger log = LoggerFactory.getLogger(SensorServiceImpl.class);

	@Autowired
	HotelState hotelState;

	@Autowired
	SensorHelper helper;

	@Override
	public void performSensorEvent(SensorInputDTO sensor) {
		log.info("Movement observed on {} floor, {} sub-corridor.", sensor.getFloor(), sensor.getSubCorridor());
		Assert.isTrue(sensor.getFloor() <= hotelState.getFloors().size(), "Floor number is invalid.");
		Assert.isTrue(
				sensor.getMainCorridor() <= hotelState.getFloors().get(sensor.getFloor()).getMainCorridors().size(),
				"Main-Corridor number is invalid.");
		Assert.isTrue(sensor.getSubCorridor() <= hotelState.getFloors().get(sensor.getFloor()).getMainCorridors().get(0)
				.getSubCorridors().size(), "Sub-Corridor number is invalid.");

		Floor currentFloor = hotelState.getFloors().get(sensor.getFloor() - 1);
		MainCorridor currentMainCorridor = currentFloor.getMainCorridors().get(sensor.getMainCorridor() - 1);
		SubCorridor currentSubCorridor = currentMainCorridor.getSubCorridors().get(sensor.getSubCorridor() - 1);

		Integer thresholdPowerConsumption = currentFloor.getPowerConsumptionThreshold();
		log.info("Threshlod Power Consumption is {}.", thresholdPowerConsumption);
		Integer currentPowerConsumption = currentFloor.getCurrentPowerConsumption();
		log.info("Current power consumption on floor {} is {}", sensor.getFloor(), currentPowerConsumption);

		log.info("Making {} light ON", currentSubCorridor.getCorridorName());
		currentSubCorridor.getLight().setValue(true);
		currentPowerConsumption = currentFloor.getCurrentPowerConsumption();
		log.info("Current power consumption after event on floor {} is {}", sensor.getFloor(), currentPowerConsumption);

		if (currentPowerConsumption > thresholdPowerConsumption) {
			log.info("First Try to turn off AC of sub corridor where there is no movement.");
			List<SubCorridor> otherSubCorridors = currentMainCorridor.getSubCorridors().stream()
					.filter(e -> e != currentSubCorridor && !e.getLight().isValue()).collect(Collectors.toList());
			if (!otherSubCorridors.isEmpty()) {
				otherSubCorridors.get(0).getAc().setValue(false);
				helper.managePowerConsumption(currentSubCorridor, otherSubCorridors.get(0));
			} else {
				log.info("Disable AC of Current sub-corridor.");
				currentSubCorridor.getAc().setValue(false);
				helper.managePowerConsumption(currentSubCorridor, currentSubCorridor);
			}
			currentPowerConsumption = currentFloor.getCurrentPowerConsumption();
			log.info("Current power consumption is {} and threshold is {}", currentPowerConsumption,
					thresholdPowerConsumption);
		} else {
			log.info("Current Power Consumption on the floor is less that Threshold Power Consumption.");
		}

	}

}
