/**
 * 
 */
package com.adevguide.assignment.hotelautomation.service;

import com.adevguide.assignment.hotelautomation.model.SensorInputDTO;

/**
 * @author PraBhu
 *
 */
public interface SensorService {

	public void performSensorEvent(SensorInputDTO sensor);

}
