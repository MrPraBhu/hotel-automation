/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author PraBhu
 *
 */
public class HotelState {

	private List<Floor> floors = new ArrayList<>();

	public List<Floor> getFloors() {
		return floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	private String listToString(List<Floor> floors) {
		StringBuilder value = new StringBuilder();
		for (Floor floor : getFloors()) {
			value.append("\n" + floor.toString());
		}
		return value.toString();
	}

	@Override
	public String toString() {
		return listToString(getFloors());
	}

}
