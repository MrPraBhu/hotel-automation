/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

import com.adevguide.assignment.hotelautomation.common.Constants;

/**
 * @author PraBhu
 *
 */

public abstract class Corridor {

	private String corridorName;
	private AC ac;
	private Light light;

	public String getCorridorName() {
		return corridorName;
	}

	public void setCorridorName(String corridorName) {
		this.corridorName = corridorName;
	}

	public AC getAc() {
		return ac;
	}

	public void setAc(AC ac) {
		this.ac = ac;
	}

	public Light getLight() {
		return light;
	}

	public void setLight(Light light) {
		this.light = light;
	}

	public Corridor(String corridorName, AC ac, Light light) {
		super();
		this.corridorName = corridorName;
		this.ac = ac;
		this.light = light;
	}

	public Corridor() {

	}

	@Override
	public String toString() {
		return getCorridorName() + " " + getLight() + " " + getAc() + "\n";
	}

	public Integer getCurrentConsumption() {
		return ((ac.isValue() ? Constants.AC_UNIT : 0) + (light.isValue() ? Constants.LIGHT_UNIT : 0));
	}

}
