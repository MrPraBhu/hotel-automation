/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

import com.adevguide.assignment.hotelautomation.common.Constants;

/**
 * @author PraBhu
 *
 */
public class SubCorridor extends Corridor {
	
	public SubCorridor(int num) {
		setCorridorName(Constants.SUB_CORRIDOR + num);
		setLight(new Light(Constants.LIGHT + num, false));
		setAc(new AC(Constants.AC + num, true));
	}



}
