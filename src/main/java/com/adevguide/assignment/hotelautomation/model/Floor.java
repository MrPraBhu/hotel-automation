/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

import java.util.ArrayList;
import java.util.List;

import com.adevguide.assignment.hotelautomation.common.Constants;

/**
 * @author PraBhu
 *
 */

public class Floor {

	private String floorName;
	private List<MainCorridor> mainCorridors = new ArrayList<>();

	public Floor(int floorNum) {
		this.floorName = Constants.FLOOR + floorNum;
	}

	public List<MainCorridor> getMainCorridors() {
		return mainCorridors;
	}

	public void setMainCorridors(List<MainCorridor> mainCorridors) {
		this.mainCorridors = mainCorridors;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	private String listToString(List<MainCorridor> floors) {
		StringBuilder value = new StringBuilder();

		for (MainCorridor corridor : getMainCorridors()) {
			value.append(corridor.toString() + "\n");
		}
		return value.toString();
	}

	public Integer getPowerConsumptionThreshold() {
		int mainCorridorCount = 0;
		int subCorridorCount = 0;
		for (MainCorridor main : getMainCorridors()) {
			mainCorridorCount++;
			subCorridorCount += main.getSubCorridors().size();
		}
		return ((mainCorridorCount * Constants.MAIN_CORRIDOR_UNIT) + (subCorridorCount * Constants.SUB_CORRIDOR_UNIT));
	}

	public Integer getCurrentPowerConsumption() {
		return getMainCorridors().stream().mapToInt(e -> e.getCurrentConsumption()).sum();
	}

	@Override
	public String toString() {
		return getFloorName() + "\n" + listToString(getMainCorridors());
	}

}
