/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

import java.util.ArrayList;
import java.util.List;

import com.adevguide.assignment.hotelautomation.common.Constants;

/**
 * @author PraBhu
 *
 */
public class MainCorridor extends Corridor {

	private List<SubCorridor> subCorridors = new ArrayList<>();

	public MainCorridor(int num) {
		super();
		setCorridorName(Constants.MAIN_CORRIDOR + num);
		setLight(new Light(Constants.LIGHT + num, true));
		setAc(new AC(Constants.AC + num, true));
	}

	public List<SubCorridor> getSubCorridors() {
		return subCorridors;
	}

	public void setSubCorridors(List<SubCorridor> subCorridors) {
		this.subCorridors = subCorridors;
	}

	public Integer getCurrentConsumption() {
		Integer currentCost = super.getCurrentConsumption();
		currentCost += subCorridors.stream().mapToInt(e -> e.getCurrentConsumption()).sum();
		return currentCost;
	}

	@Override
	public String toString() {
		StringBuilder subCor = new StringBuilder(getCorridorName() + " " + getLight() + " " + getAc() + "\n");
		for (SubCorridor sub : getSubCorridors()) {
			subCor.append(sub);
		}

		return subCor.toString();
	}

}
