/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

/**
 * @author PraBhu
 *
 */
public class SensorInputDTO {

	private int floor;
	private int mainCorridor;
	private int subCorridor;

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getSubCorridor() {
		return subCorridor;
	}

	public void setSubCorridor(int subCorridor) {
		this.subCorridor = subCorridor;
	}

	public int getMainCorridor() {
		return mainCorridor;
	}

	public void setMainCorridor(int mainCorridor) {
		this.mainCorridor = mainCorridor;
	}

}
