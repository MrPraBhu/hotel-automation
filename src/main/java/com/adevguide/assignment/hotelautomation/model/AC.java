/**
 * 
 */
package com.adevguide.assignment.hotelautomation.model;

/**
 * @author PraBhu
 *
 */
public class AC {

	private String name;
	private boolean value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public String getStatus() {
		return value ? "ON" : "OFF";
	}

	public AC(String name, boolean value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public String toString() {
		return "AC : " + getStatus();
	}

}
