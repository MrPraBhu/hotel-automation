/**
 * 
 */
package com.adevguide.assignment.hotelautomation.common;

/**
 * @author PraBhu
 *
 */
public class Constants {
	public static final String SUB_CORRIDOR = "Sub Corridor ";
	public static final String MAIN_CORRIDOR = "Main Corridor ";
	public static final String FLOOR = "Floor ";
	public static final String LIGHT = "Light ";
	public static final String AC = "AC ";

	public static final int MAIN_CORRIDOR_UNIT = 15;
	public static final int SUB_CORRIDOR_UNIT = 10;
	public static final int LIGHT_UNIT = 5;
	public static final int AC_UNIT = 10;

}
