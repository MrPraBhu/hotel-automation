package com.adevguide.assignment.hotelautomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class HotelAutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelAutomationApplication.class, args);
	}

}
