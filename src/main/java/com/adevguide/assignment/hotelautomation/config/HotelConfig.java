/**
 * 
 */
package com.adevguide.assignment.hotelautomation.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.Assert;

import com.adevguide.assignment.hotelautomation.model.Floor;
import com.adevguide.assignment.hotelautomation.model.HotelState;
import com.adevguide.assignment.hotelautomation.model.MainCorridor;
import com.adevguide.assignment.hotelautomation.model.SubCorridor;

/**
 * @author PraBhu
 *
 */
@Configuration
public class HotelConfig {
	
	

	@Bean
	@Primary
	public HotelState initializeHotel(@Value("${hotel.floors}") int floors,
			@Value("${hotel.main-corridors}") int mainCorridors, @Value("${hotel.sub-corridors}") int subCorridors) {
		HotelState hotel = new HotelState();
		Assert.isTrue(floors > 0, "Atleast 1 floor must be present.");
		Assert.isTrue(mainCorridors > 0, "Atleast 1 main corridor must be present.");
		Assert.isTrue(subCorridors > 0, "Atleast 1 sub corridor must be present.");
		Floor floor = null;
		MainCorridor mainCorridor = null;
		SubCorridor subCorridor = null;
		for (int i = 1; i <= floors; i++) {
			floor = new Floor(i);
			for (int j = 1; j <= mainCorridors; j++) {
				mainCorridor = new MainCorridor(j);
				for (int k = 1; k <= subCorridors; k++) {
					subCorridor = new SubCorridor(k);
					mainCorridor.getSubCorridors().add(subCorridor);
				}
				floor.getMainCorridors().add(mainCorridor);
			}
			hotel.getFloors().add(floor);
		}
		return hotel;
	}

}
