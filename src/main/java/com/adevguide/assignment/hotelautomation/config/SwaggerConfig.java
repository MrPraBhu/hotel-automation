/**
 * 
 */
package com.adevguide.assignment.hotelautomation.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author PraBhu
 *
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.adevguide.assignment.hotelautomation.controller"))
				.build();

	}
	
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Hotel Automation", 
	      "Some custom description of API.", 
	      "API TOS", 
	      "Terms of service", 
	      new Contact("Pratik Bhuite", "www.adevguide.com", "prabhu.sites@gmail.com"), 
	      "License of API", "API license URL", Collections.emptyList());
	}

}
