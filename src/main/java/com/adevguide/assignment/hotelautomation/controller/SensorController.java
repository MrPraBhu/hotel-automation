/**
 * 
 */
package com.adevguide.assignment.hotelautomation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adevguide.assignment.hotelautomation.model.HotelState;
import com.adevguide.assignment.hotelautomation.model.SensorInputDTO;
import com.adevguide.assignment.hotelautomation.service.SensorService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * @author PraBhu
 *
 */

@RestController
@RequestMapping("hotel")
public class SensorController {

	@Autowired
	HotelState hotelState;

	@Autowired
	SensorService sensorService;

	@GetMapping("/")
	public ResponseEntity<String> welcome() {
		return new ResponseEntity<>("Welcome", HttpStatus.OK);
	}

	@GetMapping("/status/current")
	public ResponseEntity<String> getCurrentStatus() {
		return new ResponseEntity<>(hotelState.toString(), HttpStatus.OK);
	}

	@PostMapping("/sensor/event")
	public ResponseEntity<String> triggerSensorEvent(@RequestBody SensorInputDTO sensor) {

		sensorService.performSensorEvent(sensor);

		return new ResponseEntity<>(hotelState.toString(), HttpStatus.OK);
	}

}
